# trustnet-oauth-agent-demo

This is a work-in-progress implementation of a "data access authorisation" involving OAuth and Hyperledger Indy agents. Part of Trustnet (http://trustnet.fi/).

Presentation:

https://docs.google.com/presentation/d/1RiCTuGDMJkJe6aPYwNz7SrJsg3aXYuDPWRb5mh0eK9Q/

Diagram:

See [trustnet-oauth-agent-demo.pdf](trustnet-oauth-agent-demo.pdf).

## How to build Docker images

	docker build -f ./docker/Dockerfile-base . -t trustnet-oauth-agent-demo/base && 
	docker build -f ./docker/Dockerfile-indy-jwt-bearer-token . -t trustnet-oauth-agent-demo/indy-jwt-bearer-token &&
	docker build -f ./docker/Dockerfile-trustanchor . -t trustnet-oauth-agent-demo/trustanchor &&
	docker build -f ./docker/Dockerfile-resourceserver . -t trustnet-oauth-agent-demo/resourceserver &&
	docker build -f ./docker/Dockerfile-authserver-jwt-bearer . -t trustnet-oauth-agent-demo/authserver-jwt-bearer &&
	docker build -f ./docker/Dockerfile-taito-client . -t trustnet-oauth-agent-demo/taito-client &&
	docker build -f ./docker/Dockerfile-alice-resource-owner . -t trustnet-oauth-agent-demo/alice-resource-owner

## How to run

	docker-compose -f docker-compose.yml up

## How to try

Execute a shell inside the "alice-resource-owner" Docker container:

	docker ps    <-- find ID e.g. f00e586fbcd7 of running "alice-resource-owner" Docker container
	docker exec -t -i f00e586fbcd7 /bin/bash

Run a script that will post an Indy message containing a DID-JWT delegate access credential to the "taito-client" Docker container:

	cd /opt/alice-resource-owner
	cat did-jwt-authorization-grant-message.json
	./did-jwt-authorization-grant-message-post.sh

