#!/usr/bin/env python3.6
import sys, json

DIDJWTAUTHORIZATIONGRANT = 'did:sov:BzCbsNYhMrjHiqZDTUASHg;spec/didjwt/1.0/authorizationgrant'

if len(sys.argv) != 3:
    print("Invalid parameters")
    exit(1)
token = sys.argv[1]
message = sys.argv[2]
message = json.loads(message)

if message['type'] == DIDJWTAUTHORIZATIONGRANT:
    print('Request')
else:
    print('Unknown Message Type')
    exit(1)
