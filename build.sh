#!/bin/sh
docker build -f ./docker/Dockerfile-base . -t trustnet-oauth-agent-demo/base && 
docker build -f ./docker/Dockerfile-indy-jwt-bearer-token . -t trustnet-oauth-agent-demo/indy-jwt-bearer-token &&
docker build -f ./docker/Dockerfile-trustanchor . -t trustnet-oauth-agent-demo/trustanchor &&
docker build -f ./docker/Dockerfile-resourceserver . -t trustnet-oauth-agent-demo/resourceserver &&
docker build -f ./docker/Dockerfile-authserver-jwt-bearer . -t trustnet-oauth-agent-demo/authserver-jwt-bearer &&
docker build -f ./docker/Dockerfile-taito-client . -t trustnet-oauth-agent-demo/taito-client &&
docker build -f ./docker/Dockerfile-alice-resource-owner . -t trustnet-oauth-agent-demo/alice-resource-owner

